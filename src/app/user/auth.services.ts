import { Injectable } from "@angular/core";
import { IUser } from "./user.model";
import {  of } from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import { tap, catchError } from "rxjs/operators";
@Injectable()

export class AuthService{
    currentUser:IUser

    checkAuthenticationStatus(){
     this.http.get('/api/currentIdentity')
        .pipe(tap(data =>{
            if(data instanceof Object){
                this.currentUser=<IUser>data
            }
        }))
    }
    
    constructor(private http:HttpClient){}
    loginUser(userName:string,password:string){
        let loginInfo={username:userName,password:password}
        let options={ headers: new HttpHeaders({'Content-Type':'/application/json'})}
      return  this.http.post('/api/login',loginInfo,options)
        .pipe(tap(data =>{
            this.currentUser=<IUser>data['user']
        })).pipe(catchError(err=>{
            return of(false)
        }))
      
      
         //  this.currentUser={
           //    id:1,
          //  userName:username,
    //firstName:"Albi",
         //lastName:"Luzi",
        // password:pass
        // }
    }
    isAuthentificated(){
        return !!this.currentUser;
    }

    logout(){
        this.currentUser=undefined;
        let options={ headers: new HttpHeaders({'Content-Type':'/application/json'})};
        return this.http.post('/api/logout',{},options);
    }
    updateCurrentUser(firstName:string,lastName:string){
        this.currentUser.firstName=firstName
        this.currentUser.lastName=lastName

        let options={ headers: new HttpHeaders({'Content-Type':'/application/json'})};
      return  this.http.put(`/api/user/${this.currentUser.id}`,this.currentUser,options)

    }
}